import numba
from numba import cuda
import numpy as np

import warnings

# 因为puzzle里面的规模都不大，所以使用gpu加速会导致很多threads(block?)闲置。
warnings.filterwarnings(
    action="ignore", category=numba.NumbaPerformanceWarning, module="numba"
)

def map_spec(a):
    return a + 10

@numba.jit(nopython=True)
def map_test0(a):
    return a + 10

@cuda.jit
def map_test(out, a):
    idx = cuda.threadIdx.x
    out[idx] = a[idx]+10


def zip_spec(a, b):
    return a + b

@cuda.jit
def zip_test(out, a, b):
    local_i = cuda.threadIdx.x
    out[local_i] = a[local_i] + b[local_i]

@cuda.jit
def map_guard_test(out, a, size):
    local_i = cuda.threadIdx.x
    if local_i < size:
        out[local_i] = a[local_i] + 10
        # FILL ME IN (roughly 2 lines)


# ## Puzzle 4 - Map 2D
#
# Implement a kernel that adds 10 to each position of `a` and stores it in `out`.
# Input `a` is 2D and square. You have more threads than positions.

# +

@cuda.jit
def map_2D_test(out, a, size):
    local_i = cuda.threadIdx.x
    local_j = cuda.threadIdx.y
    if local_i < size and local_j < size:
        out[local_i, local_j] = a[local_i, local_j] + 10

# ## Puzzle 5 - Broadcast
#
# Implement a kernel that adds `a` and `b` and stores it in `out`.
# Inputs `a` and `b` are vectors. You have more threads than positions.

# +
@cuda.jit
def broadcast_test(out,a,b,size):
    local_i = cuda.threadIdx.x
    local_j = cuda.threadIdx.y
    if local_i < size and local_j < size:
        out[local_i, local_j] = a[local_i,0] + b[0,local_j]

# ## Puzzle 6 - Blocks
#
# Implement a kernel that adds 10 to each position of `a` and stores it in `out`.
# You have fewer threads per block than the size of `a`.

# *Tip: A block is a group of threads. The number of threads per block is limited, but we can
# have many different blocks. Variable `cuda.blockIdx` tells us what block we are in.*

# +
@cuda.jit
def map_block_test(out,a,size):
    i = cuda.blockIdx.x * cuda.blockDim.x + cuda.threadIdx.x
    if i < size:
        out[i] = a[i] + 10


# ## Puzzle 7 - Blocks 2D
#
# Implement the same kernel in 2D.  You have fewer threads per block
# than the size of `a` in both directions.

# +
@cuda.jit
def map_block2D_test(out, a, size):
    i = cuda.blockIdx.x * cuda.blockDim.x + cuda.threadIdx.x
    j = cuda.blockIdx.y * cuda.blockDim.y + cuda.threadIdx.y
    if i < size and j < size:
        out[i,j] = a[i,j] + 10
        # FILL ME IN (roughly 4 lines)


# ## Puzzle 8 - Shared
#
# Implement a kernel that adds 10 to each position of `a` and stores it in `out`.
# You have fewer threads per block than the size of `a`.

# **Warning**: Each block can only have a *constant* amount of shared
#  memory that threads in that block can read and write to. This needs
#  to be a literal python constant not a variable. After writing to
#  shared memory you need to call `cuda.syncthreads` to ensure that
#  threads do not cross.

# (This example does not really need shared memory or syncthreads, but it is a demo.)

# +
TPB = 4
@cuda.jit
def shared_test(out, a, size):
    shared = cuda.shared.array(TPB, numba.float32)
    i = cuda.blockIdx.x * cuda.blockDim.x + cuda.threadIdx.x
    local_i = cuda.threadIdx.x

    if i < size:
        shared[local_i] = a[i]
        cuda.syncthreads()

    if i < size:    
        out[i] = shared[local_i] + 10

# ## Puzzle 9 - Pooling
#
# Implement a kernel that sums together the last 3 position of `a` and stores it in `out`.
# You have 1 thread per position. You only need 1 global read and 1 global write per thread.

# *Tip: Remember to be careful about syncing.*

# +
def pool_spec(a):
    out = np.zeros(*a.shape)
    for i in range(a.shape[0]):
        out[i] = a[max(i - 2, 0) : i + 1].sum()
    return out

# note: only one block
TPB = 8
@cuda.jit
def pool_test(out, a, size):
    shared = cuda.shared.array(TPB, numba.float32)
    i = cuda.blockIdx.x * cuda.blockDim.x + cuda.threadIdx.x
    local_i = cuda.threadIdx.x
    if i < size:
        shared[local_i] = a[i]
        cuda.syncthreads()
        out[i]=shared[local_i]
    if 1 < i and i < size:
        out[i] += shared[local_i-1]
    if 2 < i and i < size:
        out[i] += shared[local_i-2]
    # FILL ME IN (roughly 8 lines)

# a = np.arange(8)
# spec = pool_spec(a)
# test = np.zeros(8)
# pool_test[(1,1),(8,1)](test,a,8)
# print(spec)
# print(test)


# ## Puzzle 10 - Dot Product
#
# Implement a kernel that computes the dot-product of `a` and `b` and stores it in `out`.
# You have 1 thread per position. You only need 2 global reads and 1 global write per thread.

# *Note: For this problem you don't need to worry about number of shared reads. We will
#  handle that challenge later.*

# +
def dot_spec(a, b):
    return a @ b

TPB = 8
@cuda.jit
def dot_test(out, a, b, size):
    shared = cuda.shared.array(TPB, numba.float32)
    i = cuda.blockIdx.x * cuda.blockDim.x + cuda.threadIdx.x
    local_i = cuda.threadIdx.x
    
    if i < size:
        shared[local_i] += a[i] * b[i]
    # cuda.syncthreads()
    if local_i % 2 == 0 and local_i + 1 < TPB:
        shared[local_i] += shared[local_i+1]
    # cuda.syncthreads()
    if local_i % 4 == 0 and local_i + 2 < TPB:
        shared[local_i] += shared[local_i+2]
    # cuda.syncthreads()
    if local_i % 8 == 0 and local_i + 4 < TPB:
        shared[local_i] += shared[local_i+4]
    if local_i == 0:
        out[0] = shared[0]
    # FILL ME IN (roughly 9 lines)

# a = np.arange(8)
# b = np.arange(8)
# spec = dot_spec(a,b)
# test = np.zeros(1)
# dot_test[(1,1),(8,1)](test,a,b,8)
# print(spec)
# print(test)



# ## Puzzle 11 - 1D Convolution
#
# Implement a kernel that computes a 1D convolution between `a` and `b` and stores it in `out`.
# You need to handle the general case. You only need 2 global reads and 1 global write per thread.


# +
def conv_spec(a, b):
    out = np.zeros(*a.shape)
    len = b.shape[0]
    for i in range(a.shape[0]):
        out[i] = sum([a[i + j] * b[j] for j in range(len) if i + j < a.shape[0]])
    return out


MAX_CONV = 4
TPB = 8
TPB_MAX_CONV = TPB + MAX_CONV
TPB2 = 2 * TPB
@cuda.jit
def conv_test(out, a, b, a_size, b_size):
    i = cuda.blockIdx.x * cuda.blockDim.x + cuda.threadIdx.x
    local_i = cuda.threadIdx.x

    # FILL ME IN (roughly 17 lines)
    shared = cuda.shared.array(TPB2, numba.float32)
    if i < a_size:
        shared[local_i] = a[i]
    else:
        shared[local_i] = 0
    if local_i < MAX_CONV and i + TPB < a_size:
        shared[TPB + local_i] = a[i+TPB]
    elif MAX_CONV <= local_i and local_i-MAX_CONV < b_size:
        shared[TPB + local_i] = b[local_i-MAX_CONV]
    else:
        shared[TPB + local_i] = 0
    cuda.syncthreads()
    for j in range(MAX_CONV):
        out[i] += shared[TPB_MAX_CONV + j] * shared[local_i+j] 
    # out[i] = shared[local_i+3]

# # Test 1

# SIZE = 6
# CONV = 3
# test = np.zeros(SIZE)
# a = np.arange(SIZE)
# b = np.arange(CONV)
# spec = conv_spec(a,b)
# conv_test[(1,1),(TPB,1)](test,a,b,SIZE,CONV)
# print(spec)
# print(test)

# # Test 2

# # +
# test = np.zeros(15)
# a = np.arange(15)
# b = np.arange(4)
# spec = conv_spec(a,b)
# conv_test[(2,1),(TPB,1)](test,a,b,15,4)
# print(spec)
# print(test)




# ## Puzzle 12 - Prefix Sum
#
# Implement a kernel that computes a sum over `a` and stores it in `out`.
# If the size of `a` is greater than the block size, only store the sum of
# each block.

# We will do this using the [parallel prefix sum](https://en.wikipedia.org/wiki/Prefix_sum) algorithm in shared memory.
# That is, each step of the algorithm should sum together half the remaining numbers.
# Follow this diagram:

# ![](https://user-images.githubusercontent.com/35882/178757889-1c269623-93af-4a2e-a7e9-22cd55a42e38.png)

# +
# TPB = 8
# def sum_spec(a):
#     out = np.zeros((a.shape[0] + TPB - 1) // TPB)
#     for j, i in enumerate(range(0, a.shape[-1], TPB)):
#         out[j] = a[i : i + TPB].sum()
#     return out

# @cuda.jit
# def sum_test(out, a, size):
#     cache = cuda.shared.array(TPB, numba.float32)
#     i = cuda.blockIdx.x * cuda.blockDim.x + cuda.threadIdx.x
#     local_i = cuda.threadIdx.x
#     bi = cuda.blockIdx.x

#     # FILL ME IN (roughly 12 lines)
#     if i < size: 
#         cache[local_i] = a[i]
#     if local_i % 2 == 0 and i + 1 < size:
#         cache[local_i] += cache[local_i+1]
#     if local_i % 4 == 0 and i + 2 < size:
#         cache[local_i] += cache[local_i+2]
#     if local_i % 8 == 0 and i + 4 < size:
#         cache[local_i] += cache[local_i+4]
#     if local_i == 0:
#         out[bi] = cache[0]


# # Test 1

# SIZE = 8
# test = np.zeros(1)
# inp = np.arange(SIZE)
# spec = sum_spec(inp)
# sum_test[(1,1),(TPB,1)](test, inp, SIZE)
# print(spec)
# print(test)

# # Test 2

# SIZE = 15
# test = np.zeros(2)
# inp = np.arange(SIZE)
# spec = sum_spec(inp)
# sum_test[(2,1),(TPB,1)](test, inp, SIZE)
# print(spec)
# print(test)


#@todo: should i need syncthreads?


# ## Puzzle 13 - Axis Sum
#
# Implement a kernel that computes a sum over each column of `a` and stores it in `out`.

# +
# TPB = 8
# def sum_spec(a):
#     out = np.zeros((a.shape[0], (a.shape[1] + TPB - 1) // TPB))
#     for j, i in enumerate(range(0, a.shape[-1], TPB)):
#         out[..., j] = a[..., i : i + TPB].sum(-1)
#     return out

# @cuda.jit
# def axis_sum_test(out, a, size):
#     cache = cuda.shared.array(TPB, numba.float32)
#     i = cuda.blockIdx.x * cuda.blockDim.x + cuda.threadIdx.x
#     local_i = cuda.threadIdx.x
#     batch = cuda.blockIdx.y
#     # FILL ME IN (roughly 12 lines)
#     if i < size:
#         cache[local_i] = a[batch][i]
#     else:
#         cache[local_i] = 0
#     cuda.syncthreads()
#     if local_i == 0:
#         out[batch][0] = 0
#         for i in range(TPB):
#             out[batch][0] += cache[i]

# BATCH = 4
# SIZE = 6
# out = np.zeros((BATCH, 1))
# inp = np.arange(BATCH * SIZE).reshape((BATCH, SIZE)).astype(np.float32)
# print(inp)
# print(out)
# spec = sum_spec(inp)
# axis_sum_test[(1, BATCH),(TPB, 1)](out, inp, SIZE)
# print(spec)
# print(out)



# ## Puzzle 14 - Matrix Multiply!
#
# Implement a kernel that multiplies square matrices `a` and `b` and
# stores the result in `out`.
#
# *Tip: The most efficient algorithm here will copy a block into
#  shared memory before computing each of the individual row-column
#  dot products. This is easy to do if the matrix fits in shared
#  memory.  Do that case first. Then update your code to compute
#  a partial dot-product and iteratively move the part you
#  copied into shared memory.* You should be able to do the hard case
#  in 6 global reads.

# +
def matmul_spec(a, b):
    return a @ b


TPB = 3
@cuda.jit
def mm_oneblock_test(out, a, b, size):
    a_shared = cuda.shared.array((TPB, TPB), numba.float32)
    b_shared = cuda.shared.array((TPB, TPB), numba.float32)

    i = cuda.blockIdx.x * cuda.blockDim.x + cuda.threadIdx.x
    j = cuda.blockIdx.y * cuda.blockDim.y + cuda.threadIdx.y
    local_i = cuda.threadIdx.x
    local_j = cuda.threadIdx.y
    # FILL ME IN (roughly 14 lines)
    for iter in range(0,size+TPB-1, TPB):
        for local_k in range(TPB):
            k = local_k + iter
            a_shared[local_i, local_k] = a[i,k] if k < size and i < size else 0
            b_shared[local_k, local_j] = b[k,j] if k < size and j < size else 0
        cuda.syncthreads()
        if i < size and j < size:
            for local_k in range(TPB):
                out[i,j] += a_shared[local_i, local_k] * b_shared[local_k, local_j]



# Test 1

SIZE = 2
out = np.zeros((SIZE, SIZE))
inp1 = np.arange(SIZE * SIZE).reshape((SIZE, SIZE))
inp2 = np.arange(SIZE * SIZE).reshape((SIZE, SIZE)).T

spec = matmul_spec(inp1, inp2)
mm_oneblock_test[(1,1),(TPB,TPB)](out, inp1, inp2, SIZE)
print(spec)
print(out)


# Test 2

# +
SIZE = 8
out = np.zeros((SIZE, SIZE))
inp1 = np.arange(SIZE * SIZE).reshape((SIZE, SIZE))
inp2 = np.arange(SIZE * SIZE).reshape((SIZE, SIZE)).T

spec = matmul_spec(inp1, inp2)
mm_oneblock_test[(3,3),(TPB,TPB)](out, inp1, inp2, SIZE)
print(spec)
print(out)


if __name__ == "__main__":
    pass
    # a = np.arange(10)
    # b = map_spec(a)
    # c = map_test0(a)
    # print(b,c)
    # d = np.zeros(10)
    # map_test[1,10](d,a)
    # print(d)


    # a = np.arange(10)
    # b = np.arange(10)
    # spec = zip_spec(a,b)
    # test = np.zeros(10)
    # zip_test[1,10](test,a,b)
    # print(spec, test)

    # a = np.arange(10)
    # spec = map_spec(a)
    # test = np.zeros(10)
    # map_guard_test[1,20](test,a,10)
    # print(spec,test)

    # a = np.arange(10)[:,None] + np.arange(10)[None,:]
    # spec = map_spec(a)
    # test = np.zeros((10,10))
    # map_2D_test[1,(16,16)](test,a,10)
    # print(spec)
    # print(test)

    # a = np.arange(10)[:,None]
    # b = np.arange(10)[None,:]
    # spec = zip_spec(a,b)
    # test = np.zeros((10,10))
    # broadcast_test[1,(16,16)](test,a,b,10)
    # print(spec)
    # print(test)

    # a = np.arange(20)
    # spec = map_spec(a)
    # test = np.zeros(20)
    # map_block_test[8,4](test,a,20)
    # print(spec)
    # print(test)

    # a = np.arange(10)[:,None] + np.arange(10)[None,:]
    # spec = map_spec(a)
    # test = np.zeros((10,10))
    # map_block2D_test[(4,4),(4,4)](test,a,10)
    # print(spec)
    # print(test)

    # a = np.arange(20)
    # spec = map_spec(a)
    # test = np.zeros(20)
    # shared_test[8,4](test,a,20)
    # print(spec)
    # print(test)

# def map_test(cuda):
#     def call(out, a) -> None:
#         local_i = cuda.threadIdx.x

#     return call

# SIZE = 4
# out = np.zeros((SIZE,))
# a = np.arange(SIZE)
# problem = CudaProblem(
#     "Map", map_test, [a], out, threadsperblock=Coord(SIZE, 1), spec=map_spec
# )
# problem.show()