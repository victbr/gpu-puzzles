# my note for this project


## Motivation

[numba manual](https://numba.readthedocs.io/en/stable/index.html)

[srush](https://github.com/srush/GPU-Puzzles) provides `lib.py` to hide the dirty work of this puzzles. However, it is hard for me to follow, and I want to build my own tools for kernel in python. 

I notice that the original version uses `@dataclass`, which I didn't find it in `numba manual`, that's another reason why I'd like to implement my own version of GPU-Puzzles.

> update: I found that `@dataclass` is a decorator to wrap(?) a class. And inside the class, `lib.py` still use `jit` to use ecuda. 


### dataclass

dataclass类似c++的class，他提供了一些dunder的默认实现。

## Setting

### jit decorator

`@jit`: object mode, compiler 会识别他能转换成machine code的部分，然后剩下的仍然在python interpreter中执行。

`@njit, @jit(nopython=True)`: 将整个function编译成machine code。如果有numba不能识别的符号（例如Pandas），则会失败。

> notice: numba也有启动runtime的overhead。Q:为什么numba的这个launch time感觉比cuda要短？

### cuda.jit

`lib.py`作者应该用的是cuda.jit。这个更接近cuda而不是numba？像cuda一样可以设置block dim和grid dim。

[numba-cuda](https://numba.pydata.org/numba-doc/latest/cuda/kernels.html?highlight=perblock)

## puzzles

### puzzle 1: map

implement a kernel that `out = a + 10`

